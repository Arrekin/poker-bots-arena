﻿using UnityEngine;
using System.Collections;
using PokerGameData;

public static class Manager
{
    static public PlaygroundCanvasKeeper PlaygroundCanvasKeeper
    {
        get { return GameObject.FindGameObjectWithTag("PlaygroundCanvas").GetComponent<PlaygroundCanvasKeeper>(); }
    }

    static public PlaygroundManager PlaygroundManager
    {
        get { return GameObject.FindGameObjectWithTag("PlaygroundManager").GetComponent<PlaygroundManager>(); }
    }

}
