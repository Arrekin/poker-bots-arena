﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaygroundCanvasKeeper : MonoBehaviour
{
    [Serializable]
    public class AiSelectionCanvas
    {
        public GameObject MainCanvas;
        public Button Player1SelectionButton;
        public Text Player1FilepathText;
        public Button player2SelectionButton;
        public Text Player2FilepathText;
        public Toggle Player2IsHumanToggle;
        public Button StartGameButton;
        public Text ErrorMessagteText;
    }
    public AiSelectionCanvas aiSelectionCanvas;

    [Serializable]
    public class PlayZoneCanvas
    {
        [Serializable]
        public class StreamDataDisplayerCanvas
        {
            public GameObject MainCanvas;
            public Text Text; // Komponent wyświetlający logi 
        }
        public StreamDataDisplayerCanvas streamDataDisplayerCanvasPlayer1; // Główna konsola pierwszego bota
        public StreamDataDisplayerCanvas streamErrorDataDisplayerCanvasPlayer1; // Osobista konsola pierwszego bota
        public StreamDataDisplayerCanvas streamDataDisplayerCanvasPlayer2; // Główna konsola drugiego bota
        public StreamDataDisplayerCanvas streamErrorDataDisplayerCanvasPlayer2; // Osobista konsola drugiego bota
    }
    public PlayZoneCanvas playZoneCanvas;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
