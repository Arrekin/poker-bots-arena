﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class AiSelectionScript : MonoBehaviour
{
    public void OnFileSelectionButtonClick(int playerId)
    {
        switch (playerId)
        {
            case 1:
                //Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player1FilepathText.text = FileSelector.SelectFile();
                break;
            case 2:
                //Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2FilepathText.text = FileSelector.SelectFile();
                Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn = false;
                break;
        }
    }

    public void OnIsHumanToggleClick()
    {
        if (Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn)
        {
            Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2FilepathText.text = "";
        }
    }

    public void OnGameStartButtonClick()
    {
        Debug.Log("Start button Clicked!");
        Manager.PlaygroundManager.InitializeGame();
    }

    private void Start()
    {
        Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player1FilepathText.text = PlayerPrefs.GetString("PlaygroundAi1Path", "b");
        Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2FilepathText.text = PlayerPrefs.GetString("PlaygroundAi2Path", "");
        Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn = PlayerPrefs.GetInt("PlaygroundAi2IsHuman", 0) != 0;
    }
}
