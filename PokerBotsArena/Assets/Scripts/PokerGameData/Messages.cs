﻿using System.Linq;
using PokerGameData;

public class StringMessage : IMessage
{
    public readonly string data;

    public StringMessage(string data)
    {
        this.data = data;
    }

    public virtual string Get()
    {
        return data;
    }

    public virtual string GetRaw()
    {
        return data;
    }
}

public class PlayerInput : StringMessage
{
    public override string Get()
    {
        return "EDIT THID>>>>" +data;
    }

    public PlayerInput(string data) : base(data)
    {
    }
}

public class NewGameMessage : IMessage
{
    public  readonly int startChips;
    public  readonly int smallBlind;
    public  readonly int bigBlind;
    public NewGameMessage(int startChips, int smallBlind, int bigBlind)
    {
        this.startChips = startChips;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
    }
    public string Get()
    {
        return "[New game] Start chips: "+startChips+", Small blind: "+smallBlind+", Big blind: "+bigBlind;
    }

    public string GetRaw()
    {
        return "NEWGAME "+ startChips + " " + smallBlind + " " + bigBlind;
    }
}

public class GameErrorMessage : IMessage
{
    public readonly Global.ErrorType errorType;
    public readonly IPokerPlayer caster;

    public GameErrorMessage(Global.ErrorType error, IPokerPlayer caster = null)
    {
        errorType = error;
        this.caster = caster;
    }
    public string Get()
    {
        const string ret = "Error occured: ";
        switch (errorType)
        {
            case Global.ErrorType.WrongId:
                return ret + "Player [" + GetName() + "] has wrong Id!";
            case Global.ErrorType.Timeout:
                return ret + "Hands number exceeded. Game Terminated";
            case Global.ErrorType.EmptyMessage:
                return ret + "Player [" + GetName() + "] sent empty message!";
            case Global.ErrorType.UnknowCommand:
                return ret + "Player [" + GetName() + "] sen unknow command!";
            default:
                return ret + "Unknow Error, caster: " + GetName();
        }
    }

    public string GetRaw()
    {
        return "ERROR " + errorType;
    }

    protected string GetName()
    {
        return caster == null ? "?null?" : caster.Name;
    }
}

public class MessageUnknowCommand : StringMessage
{
    public override string Get()
    {
        return "[Game Feedback] Unknow Command: " + data;
    }

    public MessageUnknowCommand(string data) : base(data)
    {
    }
}
public class MessageCheckForbidden: IMessage
{
    public string Get()
    {
        return "[Game Feedback] Check Forbidden! (pots not equal) --> Changed to Fold";
    }

    public string GetRaw()
    {
        throw new System.NotImplementedException();
    }
}

public class MessageNotEnaughChipsToRaise : IMessage
{
    public string Get()
    {
        return "[Game Feedback] Not Enaugh Chips to Raise! --> Changed to Call";
    }

    public string GetRaw()
    {
        throw new System.NotImplementedException();
    }
}



public class NewHandMessage : IMessage
{
    public readonly Card card1;
    public readonly Card card2;

    public NewHandMessage(Card card1, Card card2)
    {
        this.card1 = card1;
        this.card2 = card2;
    }
    public string Get()
    {
        return "New Hand: " + card1 + " " + card2;
    }

    public string GetRaw()
    {
        return "NEWHAND " + card1 + " " + card2;
    }
}

public class SmallBlindMessage : IMessage
{
    public readonly int blind;
    public readonly int pot;
    public SmallBlindMessage(int blind, int pot)
    {
        this.blind = blind;
        this.pot = pot;
    }
    public virtual string Get()
    {
        return "SmallBlind: " + blind + ", Pot: " + pot;
    }

    public virtual string GetRaw()
    {
        return "SMALLBLIND " + blind + " " + pot;
    }
}

public sealed class BigBlindMessage : SmallBlindMessage
{
    public BigBlindMessage(int blind, int pot) : base(blind, pot)
    {
    }
    public override string Get()
    {
        return "BigBlind: " + blind + ", Pot: " + pot;
    }

    public override string GetRaw()
    {
        return "BIGBLIND " + blind + " " + pot;
    }
}

public sealed class CallMessage : IMessage
{
    private readonly int pot;
    private readonly bool reactionAviable;
    private readonly string playerName;

    public CallMessage(string playerName, int pot, bool reactionAviable)
    {
        this.pot = pot;
        this.reactionAviable = reactionAviable;
        this.playerName = playerName;
    }
    public string Get()
    {
        return "GAME: [" + playerName + "] called, Pot: " + pot + ", Reaction " + (reactionAviable ? "" : "not ") + "aviable";
    }

    public string GetRaw()
    {
        return "CALL "+ playerName +" "+ pot + " " + reactionAviable;
    }
}
public sealed class RaiseMessage : IMessage
{
    private readonly int pot;
    private readonly string playerName;

    public RaiseMessage(string playerName, int pot)
    {
        this.pot = pot;
        this.playerName = playerName;
    }
    public string Get()
    {
        return "GAME: [" + playerName + "] raised, Pot: " + pot;
    }

    public string GetRaw()
    {
        return "RAISE " + playerName + " " + pot;
    }
}
public sealed class FoldMessage : IMessage
{
    private readonly string whoFolded;

    public FoldMessage(string whoFolded)
    {
        this.whoFolded = whoFolded;
    }
    public string Get()
    {
        return "GAME: [" + whoFolded + "] folded!";
    }

    public string GetRaw()
    {
        return "FOLD " + whoFolded;
    }
}
public sealed class CheckMessage : IMessage
{
    private readonly string whoChecked;
    private readonly bool reactionAviable;

    public CheckMessage(string whoChecked, bool reactionAviable)
    {
        this.whoChecked = whoChecked;
        this.reactionAviable = reactionAviable;
    }
    public string Get()
    {
        return "GAME: [" + whoChecked + "] checked! "+(reactionAviable?"Reaction aviable!":"");
    }

    public string GetRaw()
    {
        return "CHECK " + whoChecked + " " + reactionAviable;
    }
}

public sealed class ResolveMessage : IMessage
{
    private readonly int pot;
    private readonly bool isTie;
    private readonly string winner;

    public ResolveMessage(string winner, int pot)
    {
        this.pot = pot;
        this.winner = winner;
        isTie = false;
    }
    public ResolveMessage(int pot)
    {
        this.pot = pot;
        isTie = true;
    }
    public string Get()
    {
        return isTie ? "GAME: Tie, both players receive from pot: " + pot : "GAME: [" + winner + "] won pot: " + pot;
    }

    public string GetRaw()
    {
        return isTie ? "RESOLVETIE " + pot : "RESOLVE " + winner + " " + pot;
    }
}

public sealed class FlopMessage : IMessage
{
    private readonly string playerToAct;
    private readonly Card[] flop;

    public FlopMessage(Card[] flop, string playerToAct)
    {
        this.flop = flop;
        this.playerToAct = playerToAct;
        
    }
    public string Get()
    {
        return "GAME: Flop cards: "+flop[0] + " "+flop[1]+" " +flop[2]+", now acting ["+playerToAct+"]";
    }

    public string GetRaw()
    {
        return "FLOP " + flop[0] + " " + flop[1] + " " + flop[2] + " " + playerToAct;
    }
}
public sealed class TurnMessage : IMessage
{
    private readonly string playerToAct;
    private readonly Card turn;

    public TurnMessage(Card turn, string playerToAct)
    {
        this.turn = turn;
        this.playerToAct = playerToAct;

    }
    public string Get()
    {
        return "GAME: Turn card: " + turn + ", now acting [" + playerToAct + "]";
    }

    public string GetRaw()
    {
        return "TURN " + turn + " " + playerToAct;
    }
}

public sealed class RiverMessage : IMessage
{
    private readonly string playerToAct;
    private readonly Card river;

    public RiverMessage(Card river, string playerToAct)
    {
        this.river = river;
        this.playerToAct = playerToAct;

    }
    public string Get()
    {
        return "GAME: River card: " + river + ", now acting [" + playerToAct + "]";
    }

    public string GetRaw()
    {
        return "RIVER " + river + " " + playerToAct;
    }
}

public class BlindsHistory: IMessage
{
    public int smallBlind;
    public int bigBlind;
    public int pot;

    public BlindsHistory(int smallBlind, int bigBlind, int pot)
    {
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.pot = pot;
    }
    public string Get()
    {
        return "Small Blind: " + smallBlind + ", Big Blind: " + bigBlind + ", Pot: " + pot;
    }

    public string GetRaw()
    {
        throw new System.NotImplementedException();
    }
}

public class CallDecision : StringMessage
{
    private readonly IPokerPlayer caster;
    public override string Get()
    {
        return "["+caster.Name+"]: Call";
    }

    public CallDecision(IPokerPlayer caster) : base("CALL")
    {
        this.caster = caster;
    }
}
public class CheckDecision : StringMessage
{
    private readonly IPokerPlayer caster;
    public override string Get()
    {
        return "[" + caster.Name + "]: Check";
    }

    public CheckDecision(IPokerPlayer caster) : base("CHECK")
    {
        this.caster = caster;
    }
}

public class FoldDecision : StringMessage
{
    private readonly IPokerPlayer caster;
    public override string Get()
    {
        return "[" + caster.Name + "]: Folded";
    }

    public FoldDecision(IPokerPlayer caster) : base("FOLD")
    {
        this.caster = caster;
    }
}

public class RaiseDecision : StringMessage
{
    private readonly IPokerPlayer caster;
    private readonly int raiseValue;
    public override string Get()
    {
        return "[" + caster.Name + "]: Raise to "+raiseValue;
    }

    public RaiseDecision(IPokerPlayer caster, int value) : base("RAISE "+value)
    {
        this.caster = caster;
        this.raiseValue = value;
    }
}

public class ShowdownMessage : IMessage
{
    private readonly string cards;
    private readonly string type;
    private readonly IPokerPlayer player;

    public ShowdownMessage(string cards, string type, IPokerPlayer player)
    {
        this.cards = cards;
        this.type = type;
        this.player = player;
    }
    public string Get()
    {
        return "Showdown[" + player.Name + "]: " + cards + " | " + type;
    }

    public string GetRaw()
    {
        return "SHOWDOWN " + player.Name + " " + type.Replace(" ", "") + " " + cards;
    }
}

public class GameEndMessage : IMessage
{
    private readonly IPokerPlayer winner;

    public GameEndMessage(IPokerPlayer winner)
    {
        this.winner = winner;
    }
    public string Get()
    {
        return "GAME: Player [" + winner.Name + "] won!";
    }

    public string GetRaw()
    {
        return "GAMEEND " + winner.Name;
    }
}