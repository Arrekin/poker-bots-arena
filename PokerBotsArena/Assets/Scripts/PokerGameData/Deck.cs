﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace PokerGameData
{
    public class Deck
    {
        /// <summary>
        /// Contains default deck. Itis generated when deck instance is used for the first time
        /// </summary>
        private static List<Card> _defaultDeck;

        /// <summary>
        /// Returns default deck version as read only list
        /// </summary>
        protected ReadOnlyCollection<Card> DefaultDeck
        {
            get
            {
                if( _defaultDeck == null ) GenerateDefaultDeck();
                return _defaultDeck.AsReadOnly();
            }
        }

        /// <summary>
        /// cards for this deck
        /// </summary>
        protected List<Card> cards;
        /// <summary>
        /// Points to cart to be returned in next draw
        /// </summary>
        protected int pointer = 0;

        public Deck()
        {
            cards = new List<Card>(DefaultDeck);
            ShuffleDeck();
            /*foreach (var card in cards)
            {
                Debug.Log(card.ToString());
            }*/
        }

        public Card GetNextCard()
        {
            return cards[pointer++];
        }

        protected void ShuffleDeck()
        {
            for (int i = 0; i < cards.Count; i++)
            {
                Card temp = cards[i];
                int randomIndex = Random.Range(i, cards.Count);
                cards[i] = cards[randomIndex];
                cards[randomIndex] = temp;
            }
        }


        private static void GenerateDefaultDeck()
        {
            _defaultDeck = new List<Card>();
            for (int value = 2; value <= 14; ++value)
            {
                for (int color = 0; color < 4; ++color)
                {
                    _defaultDeck.Add(new Card(value,color));
                }
            }
        }
    }
}
