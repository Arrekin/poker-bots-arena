﻿using System.Collections.Generic;
using System.Linq;
using PokerGameData.Players;
using UnityEngine;

namespace PokerGameData
{
    public class PlaygroundManager : MonoBehaviour
    {
        private List<IPokerPlayer> players;
        private PokerGame currentGame;
        public void InitializeGame()
        {
            SavePlayerPrefs();
            if (Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player1FilepathText.text == "" ||
                (!Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn &&
                 Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2FilepathText.text == ""))
            {
                Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.ErrorMessagteText.text = "Bot's setup not ready!";
                return;
            }


            players = new List<IPokerPlayer>();
            players.Add(new RandomPlayer("CPU_1"));
            if (!Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn)
            {
                players.Add(new RandomPlayer("CPU_2"));
            }
            currentGame = new PokerGame();
            currentGame.RegisterPlayer(players[0], Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamDataDisplayerCanvasPlayer1.Text,Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamErrorDataDisplayerCanvasPlayer1.Text);
            currentGame.RegisterPlayer(players[1], Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamDataDisplayerCanvasPlayer2.Text, Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamErrorDataDisplayerCanvasPlayer2.Text);
            currentGame.StartGame();
            Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.MainCanvas.SetActive(false);
        }

        // Update is called once per frame
        /*private void OnGUI ()
        {
            if (players == null) return;
            if (players[0].NeedUpdate)
            {
                while (players[0].Input.Count > 0)
                {
                    players[0].Game.NewPlayerInput(0,players[0].Input.Dequeue().GetRaw());
                }
                Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamDataDisplayerCanvasPlayer1.Text.text = players[0].Data;
                Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamErrorDataDisplayerCanvasPlayer1.Text.text = players[0].ErrorData;
            }
            if (players[1].NeedUpdate)
            {
                Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamDataDisplayerCanvasPlayer2.Text.text = players[1].Data;
                Manager.PlaygroundCanvasKeeper.playZoneCanvas.streamErrorDataDisplayerCanvasPlayer2.Text.text = players[1].ErrorData;
            }
        }*/

        private void Update()
        {
            if( currentGame != null) currentGame.Update();
        }

        private static void SavePlayerPrefs()
        {
            PlayerPrefs.SetString("PlaygroundAi1Path",Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player1FilepathText.text);
            PlayerPrefs.SetString("PlaygroundAi2Path", Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2FilepathText.text);
            PlayerPrefs.SetInt("PlaygroundAi2IsHuman", Manager.PlaygroundCanvasKeeper.aiSelectionCanvas.Player2IsHumanToggle.isOn ? 1 : 0);
            PlayerPrefs.Save();
        }
    }
}
