﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PokerGameData;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class PokerGame : IPokerGame
{
    /*protected delegate void OnActionHandler(PokerGame sender, TimeoutArgs args);

    protected event OnActionHandler OnTimeout;*/

    public readonly LinkedList<IMessage> History = new LinkedList<IMessage>();
    public readonly List<Queue<IMessage>> Input;
    #region Game Setup

    public int startChips;
    public int handsPerBlind;
    public int handsLeft;

    #endregion
    protected int playersNumber;
    protected readonly List<Player> players;
    protected readonly List<int> playerPot;
    protected int neutralPot;
    protected int handsTillBlindsChange;

    protected int currentPlayer;
    protected int currentStartingPlayer; // Player on small blind
    protected int currentSmallBlind;
    protected int currentBigBlind;
    protected int currentBlindLevel;
    protected Card[] board;

    protected int CurrentPot
    {
        get { return playerPot[0] + playerPot[1] + neutralPot; }
    }
    protected Deck currentDeck;
    protected bool actionLock;
    protected bool active; //is game currently running
    protected int stage;

    protected int[] smallBlindStructure =
    {
        // Structure from http://www.fulltilt.com/poker/tournaments/structures/heads-up
        10, 15, 20, 25, 30, 40, 50, 60, 80, 100, 120, 150, 200, 250, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000, 2500,
        3000, 4000
    };

    public PokerGame( int startChips = 1000, int handsPerBlind = 3)
    {
        this.startChips = startChips;
        this.handsPerBlind = handsPerBlind;
        currentBlindLevel = 0;
        currentSmallBlind = smallBlindStructure[currentBlindLevel];
        currentBigBlind = smallBlindStructure[currentBlindLevel]*2;
        handsLeft = 100;
        players = new List<Player>();
        playerPot = new List<int>();
        Input = new List<Queue<IMessage>>();
    }

    public void RegisterPlayer(IPokerPlayer player, Text mainUiRef, Text playerUiRef)
    {
        var newPlayer = new Player(player,players.Count,startChips, mainUiRef, playerUiRef);
        players.Add(newPlayer);
        playerPot.Add(0);
        player.Game = this;
    }

    public void StartGame()
    {
        playersNumber = players.Count;
        neutralPot = 0;
        var msg = new NewGameMessage(startChips, currentSmallBlind, currentBigBlind);
        History.AddLast(msg);
        SendToAll(msg);
        currentStartingPlayer = 0;
        handsTillBlindsChange = handsPerBlind;
        board = new Card[5];
        actionLock = false;
        active = true;
        DistributeCards();
    }

    public void Update()
    {
        if (!active) return;

        if (handsLeft <= 0)
        {
            Timeout();
            return;
        }

        IMessage msg = players[currentPlayer].GetInput();
        if (msg != null)
        {
            string rawDecision = msg.GetRaw();
            string[] decision = rawDecision.Split(' ');
            if (decision.Length < 1)
            {
                ErrorEmptyMessage(players[currentPlayer].Original);
                return;
            }
            switch (decision[0])
            {
                case "FOLD":
                    players[currentPlayer].AddToMainConsole(msg);
                    Resolve(players[NextPlayer()]);
                    break;
                case "CALL":
                    players[currentPlayer].AddToMainConsole(msg);
                    playerPot[currentPlayer] += TakeChips(players[currentPlayer], Mathf.Max(0,playerPot[NextPlayer()]-playerPot[currentPlayer]));
                    if (playerPot[currentPlayer] < playerPot[NextPlayer()] || players[currentPlayer].Chips == 0) //SmallBlind player dont have enaugh chips
                    {
                        var difference = playerPot[NextPlayer()] - playerPot[currentPlayer];
                        GiveChips(players[NextPlayer()], difference);
                        playerPot[NextPlayer()] = playerPot[currentPlayer];
                        var effect = new CallMessage(players[currentPlayer].Original.Name, CurrentPot, false);
                        History.AddLast(effect);
                        SendToAll(effect);
                        ProceedNextStage();
                    }
                    else
                    {
                        var effect = new CallMessage(players[currentPlayer].Original.Name, CurrentPot, stage==0);
                        History.AddLast(effect);
                        SendToAll(effect);
                        if (CurrentPot > 2*currentBigBlind + 1 || stage != 0)
                        {
                            ProceedNextStage();
                        }
                        else currentPlayer = NextPlayer();
                    }
                    break;
                case "RAISE":
                    players[currentPlayer].AddToMainConsole(msg);
                    var value = int.Parse(decision[1]);
                    playerPot[currentPlayer] += TakeChips(players[currentPlayer], Mathf.Max(0, value - playerPot[currentPlayer]));
                    if (playerPot[currentPlayer] < playerPot[NextPlayer()] || players[currentPlayer].Chips == 0) // not enaugh chips to rise, switching to call
                    {
                        ErrorNotEnaughChipsForRaise(players[currentPlayer]);
                        var difference = playerPot[NextPlayer()] - playerPot[currentPlayer];
                        GiveChips(players[NextPlayer()], difference);
                        playerPot[NextPlayer()] = playerPot[currentPlayer];
                        var effect = new CallMessage(players[currentPlayer].Original.Name, CurrentPot, false);
                        History.AddLast(effect);
                        SendToAll(effect);
                        ProceedNextStage();
                    }
                    else if (playerPot[currentPlayer] == playerPot[NextPlayer()]) // not enaugh value bet but it is first player so it cant be call
                    {
                        var cm = new CheckMessage(players[currentPlayer].Original.Name, true);
                        History.AddLast(cm);
                        SendToAll(cm);
                        currentPlayer = NextPlayer();
                    }
                    else
                    {
                        var raise = new RaiseMessage(players[currentPlayer].Original.Name,CurrentPot);
                        History.AddLast(raise);
                        SendToAll(raise);
                        currentPlayer = NextPlayer();
                    }
                    break;
                case "CHECK":
                    players[currentPlayer].AddToMainConsole(msg);
                    if (playerPot[currentPlayer] < playerPot[NextPlayer()]) // check not allowed with current conditions
                    {
                        ErrorCheckForbidden(players[currentPlayer]);
                        Fold();
                    }
                    else
                    {
                        if (stage == 0 || currentPlayer == currentStartingPlayer)
                        {
                            var cm = new CheckMessage(players[currentPlayer].Original.Name, false);
                            History.AddLast(cm);
                            SendToAll(cm);
                            ProceedNextStage();
                        }
                        else
                        {
                            var cm = new CheckMessage(players[currentPlayer].Original.Name, true);
                            History.AddLast(cm);
                            SendToAll(cm);
                            currentPlayer = NextPlayer();
                        }
                    }
                    break;
                default:
                    ErrorUnknowCommand(players[currentPlayer],decision[0]);
                    currentPlayer = ++currentPlayer % playersNumber;
                    return;
            }
        }
    }

    protected void DistributeCards()
    {
        players[0].AddToPlayerConsole(new StringMessage("dealing new cards, hands left: "+handsLeft+ "p1 chips: "+ players[0].Chips +  ", p2 chips: "+players[1].Chips));
        currentPlayer = currentStartingPlayer;
        currentDeck = new Deck();
        foreach (var player in players)
        {
            Card c1 = currentDeck.GetNextCard();
            Card c2 = currentDeck.GetNextCard();
            var msg = new NewHandMessage(c1, c2);
            History.AddLast(msg);
            player.SetHand(c1,c2,msg);
        }

        if (handsTillBlindsChange == 0)
        {
            ++currentBlindLevel;
            currentSmallBlind = smallBlindStructure[currentBlindLevel];
            currentBigBlind = smallBlindStructure[currentBlindLevel] * 2;
            handsTillBlindsChange = handsPerBlind;
        }
        var smallBlind = TakeChips(players[currentPlayer], currentSmallBlind);
        var bigBlind = TakeChips(players[NextPlayer()], currentBigBlind);

        playerPot[currentPlayer] = smallBlind;
        playerPot[NextPlayer()] = bigBlind;

        var sbMsg = new SmallBlindMessage(smallBlind, CurrentPot);
        SendToPlayer(players[currentPlayer], sbMsg);
        var bbMsg = new BigBlindMessage(bigBlind, CurrentPot);
        SendToPlayer(players[NextPlayer()],bbMsg);

        //save to history
        History.AddLast(new BlindsHistory(smallBlind, bigBlind, CurrentPot));
        stage = 0; //reset game stage
        handsLeft -= 1;
        handsTillBlindsChange -= 1;
    }

    protected void ProceedNextStage()
    {
        stage+=1;
        switch (stage)
        {
            case 1: // flop
                board[0] = currentDeck.GetNextCard();
                board[1] = currentDeck.GetNextCard();
                board[2] = currentDeck.GetNextCard();
                currentPlayer = NextStartingPlayer();
                var fm = new FlopMessage(new [] {board[0], board[1], board[2]}, players[currentPlayer].Original.Name);
                History.AddLast(fm);
                SendToAll(fm);
                break;
            case 2: // turn
                board[3] = currentDeck.GetNextCard();
                currentPlayer = NextStartingPlayer();
                var tm = new TurnMessage(board[3],players[currentPlayer].Original.Name);
                History.AddLast(tm);
                SendToAll(tm);
                break;
            case 3: // river
                board[4] = currentDeck.GetNextCard();
                currentPlayer = NextStartingPlayer();
                var rm = new RiverMessage(board[4], players[currentPlayer].Original.Name);
                History.AddLast(rm);
                SendToAll(rm);
                break;
            case 4: //showdown
                Showdown();
                break;

        }
    }

    protected void Showdown()
    {
        var p1Cards = GetSortedCards(players[0]);
        var p2Cards = GetSortedCards(players[1]);

        var p1Best = GetWinningCombination(p1Cards);
        var p2Best = GetWinningCombination(p2Cards);

        var outcome = p1Best.CompareWith(p2Best);

        var sm = new ShowdownMessage(p1Best.GetCards(),p1Best.GetType(),players[0].Original);
        History.AddLast(sm);
        SendToAll(sm);
        sm = new ShowdownMessage(p2Best.GetCards(), p2Best.GetType(), players[1].Original);
        History.AddLast(sm);
        SendToAll(sm);

        switch (outcome)
        {
            case Global.Outcome.Tie:
                Resolve(null);
                break;
            case Global.Outcome.Win:
                Resolve(players[0]);
                break;
            default:
                Resolve(players[1]);
                break;
        }

    }

    protected Card[] GetSortedCards(Player player)
    {
        var toRet = new Card[7];
        toRet[0] = board[0];
        toRet[1] = board[1];
        toRet[2] = board[2];
        toRet[3] = board[3];
        toRet[4] = board[4];
        toRet[5] = player.Hand[0];
        toRet[6] = player.Hand[1];
        Array.Sort<Card>(toRet, (x,y)=>y.value.CompareTo(x.value));
        return toRet;
    }

    protected WinningCombination GetWinningCombination(Card[] cards)
    {
        //Debug.Log(cards.Aggregate("", (current, card) => current + card.ToString()));
        // First Search for Straight Flush
        for (var i = 0; i < 3; ++i)
        {
            var found = new List<int> { i };
            for (var j = i+1; j < 7; ++j)
            {
                if (cards[found.Count - 1].value - cards[j].value != 1 || cards[found.Count - 1].color != cards[j].color) continue;
                found.Add(j);
                if (found.Count == 5) break;
            }
            if (found.Count!=5) continue;
            var winCards = new[]{ cards[found[0]], cards[found[1]], cards[found[2]], cards[found[3]], cards[found[4]]};
            return new WinningCombination(WinningCombination.CombinationType.StraightFlush, winCards);
        }
        // Next search for 4 of a kind
        for (var i = 0; i < 4; ++i)
        {
            var found = true;
            for (var j = i + 1; j < i + 4; ++j)
            {
                if (cards[j - 1].value - cards[j].value == 0) continue;
                found = false;
                break;
            }
            if (!found) continue;
            var highCard = i == 0 ? cards[4] : cards[0];
            var winCards = new[] { cards[i], cards[i + 1], cards[i + 2], cards[i + 3], highCard };
            return new WinningCombination(WinningCombination.CombinationType.FourOfaKind, winCards);
        }
        // Full House
        for (var i = 0; i < 5; ++i)
        {
            //search for 3 same cards
            var found = true;
            for (var j = i + 1; j < i + 3; ++j)
            {
                if (cards[j - 1].value - cards[j].value == 0) continue;
                found = false;
                break;
            }
            if (!found) continue;
            //search for 2 same cards
            found = false;
            int k;
            for ( k = 0; k < 6; ++k)
            {
                if (k >= i && k < i + 3) continue;
                if (cards[k].value - cards[k + 1].value != 0) continue;
                found = true;
                break;
            }
            if (!found) continue;
            var winCards = new[] { cards[i], cards[i + 1], cards[i + 2], cards[k], cards[k+1]};
            return new WinningCombination(WinningCombination.CombinationType.FullHouse, winCards);
        }
        // Flush
        for (var i = 0; i < 3; ++i)
        {
            var found = new List<int> {i};
            for (var j = i + 1; j < 7; ++j)
            {
                if( cards[found[0]].color != cards[j].color) continue;
                found.Add(j);
                if(found.Count==5) break;
            }
            if (found.Count!=5) continue;
            var winCards = new[] { cards[found[0]], cards[found[1]], cards[found[2]], cards[found[3]], cards[found[4]] };
            return new WinningCombination(WinningCombination.CombinationType.Flush, winCards);
        }
        // Straight
        for (var i = 0; i < 3; ++i)
        {
            var found = new List<int> { i };
            for (var j = i + 1; j < 7; ++j)
            {
                if (cards[found.Count - 1].value - cards[j].value != 1 ) continue;
                found.Add(j);
                if (found.Count == 5) break;
            }
            if (found.Count != 5) continue;
            var winCards = new[] { cards[found[0]], cards[found[1]], cards[found[2]], cards[found[3]], cards[found[4]] };
            return new WinningCombination(WinningCombination.CombinationType.Straight, winCards);
        }
        // Three of a kind
        for (var i = 0; i < 5; ++i)
        {
            //search for 3 same cards
            var found = true;
            for (var j = i + 1; j < i + 3; ++j)
            {
                if (cards[j - 1].value - cards[j].value == 0) continue;
                found = false;
                break;
            }
            if (!found) continue;
            int hk1=-1, hk2=-1;
            for (var k = 0; k < 7; ++k)
            {
                if (k >= i && k < i + 3) continue;
                if (hk1 == -1)
                    hk1 = k;
                else if (hk2 == -1)
                {
                    hk2 = k;
                    break;
                }
            }
            var winCards = new[] { cards[i], cards[i + 1], cards[i + 2], cards[hk1], cards[hk2] };
            return new WinningCombination(WinningCombination.CombinationType.ThreeOfaKind, winCards);
        }
        // Two Pairs
        int p1=-1, p2=-1, hk=0;
        for (var i = 0; i < 6; ++i)
        {
            if (cards[i].value - cards[i + 1].value != 0) continue;
            if (p1 == -1)
                p1 = i;
            else if (p2 == -1)
            {
                p2 = i;
                for (var k = 0; k < 7; ++k)
                {
                    if (k == p1 || k == p1 + 1 || k == p2 || k == p2 + 1) continue;
                    hk = k;
                    break;
                }
                break;
            }
        }
        if (p1 != -1 && p2 != -1)
        {
            var winCards = new[] { cards[p1], cards[p1+1], cards[p2], cards[p2+1], cards[hk] };
            return new WinningCombination(WinningCombination.CombinationType.TwoPair, winCards);
        }
        // One Pair
        for (var i = 0; i < 6; ++i)
        {
            if (cards[i].value - cards[i + 1].value != 0) continue;
            int hk1 = -1, hk2 = -1, hk3 = -1;
            for (var k = 0; k < 7; ++k)
            {
                if (k == i || k == i + 1) continue;
                if (hk1 == -1)
                    hk1 = k;
                else if (hk2 == -1)
                    hk2 = k;
                else if (hk3 == -1)
                {
                    hk3 = k;
                    break;
                }
            }
            var winCards = new[] { cards[i], cards[i+1], cards[hk1], cards[hk2], cards[hk3] };
            return new WinningCombination(WinningCombination.CombinationType.OnePair, winCards);
        }
        // High Card
        var highCards = new[] { cards[0], cards[1], cards[2], cards[3], cards[4] };
        return new WinningCombination(WinningCombination.CombinationType.HighCard, highCards);
    }

    protected void Fold()
    {
        var fm = new FoldMessage(players[currentPlayer].Original.Name);
        History.AddLast(fm);
        SendToAll(fm);
        var winner = NextPlayer();
        Resolve(players[winner]);
    }

    protected void Resolve(Player winner)
    {
        if (winner == null) //tie split pot
        {
            int half = CurrentPot/2;
            neutralPot = CurrentPot - 2*half;
            GiveChips(players[0], half);
            GiveChips(players[1], half);
            var rm = new ResolveMessage(half);
            History.AddLast(rm);
            SendToAll(rm);
        }
        else
        {
            GiveChips(winner, CurrentPot);
            var rm = new ResolveMessage(winner.Original.Name, CurrentPot);
            History.AddLast(rm);
            SendToAll(rm);
            neutralPot = 0;
        }
        currentStartingPlayer = NextStartingPlayer();

        if (players[0].Chips <= 0 || players[1].Chips <= 0)
        {
            var gameWinner = players[0].Chips <= 0 ? players[1].Original: players[0].Original;
            var geMsg = new GameEndMessage(gameWinner);
            History.AddLast(geMsg);
            SendToAll(geMsg);
            active = false;
            return;
        }

        DistributeCards();
    }

    protected int TakeChips(Player player, int amount)
    {
        var toTake = Mathf.Min(amount, player.Chips);
        player.Chips -= toTake;
        return toTake;
    }

    protected void GiveChips(Player player, int amount)
    {
        player.Chips += amount;
    }

    protected int NextPlayer()
    {
        return (currentPlayer + 1)%players.Count;
    }

    protected int NextStartingPlayer()
    {
        return (currentStartingPlayer + 1) % players.Count;
    }

    protected void Timeout()
    {
        var msg = new GameErrorMessage(Global.ErrorType.Timeout,players[currentPlayer].Original);
        History.AddLast(msg);
        SendToAll(msg);
        active = false;
    }

    protected void ErrorEmptyMessage(IPokerPlayer player)
    {
        var msg = new GameErrorMessage(Global.ErrorType.EmptyMessage,player);
        History.AddLast(msg);
        SendToAll(msg);
        active = false;
    }

    protected void ErrorUnknowCommand(Player player, string command)
    {
        var msg = new GameErrorMessage(Global.ErrorType.UnknowCommand, player.Original);
        History.AddLast(msg);
        SendToAll(msg);

        var ueMsg = new MessageUnknowCommand(command);
        player.AddToPlayerConsole(ueMsg);

        active = false;
    }

    protected void ErrorCheckForbidden(Player player)
    {
        var cfMsg = new MessageCheckForbidden();
        player.AddToPlayerConsole(cfMsg);
    }

    protected void ErrorNotEnaughChipsForRaise(Player player)
    {
        var nectrMsg = new MessageNotEnaughChipsToRaise();
        player.AddToPlayerConsole(nectrMsg);
    }

    protected void SendToAll( IMessage message)
    {
        foreach (var player in players)
        {
            player.AddToMainConsole(message);
            player.Original.ReceiveMessage(message);
        }
    }

    protected void SendToPlayer(Player player, IMessage message)
    {
        player.AddToMainConsole(message);
        player.Original.ReceiveMessage(message);
    }

    public void ReceiveMessage(IPokerPlayer player, IMessage message)
    {
        var inPlayer = GetPlayer(player);
        if ( inPlayer == null)
        {
            var msg = new GameErrorMessage(Global.ErrorType.WrongId, player);
            History.AddLast(msg);
            SendToAll(msg);
        }
        else
        {
            inPlayer.AddInput(message);
        }
    }

    public void ReceiveLogMessage(IPokerPlayer player, IMessage message)
    {
        var inPlayer = GetPlayer(player);
        if (inPlayer == null)
        {
            var msg = new GameErrorMessage(Global.ErrorType.WrongId, player);
            History.AddLast(msg);
            SendToAll(msg);
        }
        else
        {
            inPlayer.AddToPlayerConsole(message);
        }
    }

    protected Player GetPlayer(IPokerPlayer player)
    {
        return players.FirstOrDefault(p => p.Original == player);
    }


    public sealed class Player
    {
        public int Id { get; private set; }
        public int Chips { get; set; }
        public IPokerPlayer Original { get; private set; }

        public Card[] Hand { get; private set; }

        private readonly Queue<IMessage> input;
        private readonly Console mainConsole;
        private readonly Console playerConsole;

        public Player(IPokerPlayer original, int id, int chips, Text mainUiRef, Text playerUiRef)
        {
            Id = id;
            Chips = chips;
            Original = original;
            input = new Queue<IMessage>();
            mainConsole = new Console(mainUiRef);
            playerConsole = new Console(playerUiRef);
        }

        public void SetHand(Card card1, Card card2, NewHandMessage msg)
        {
            Hand = new[] {card1, card2};
            Original.ReceiveMessage(msg);
            AddToMainConsole(msg);
        }

        public void AddToMainConsole(IMessage msg)
        {
            mainConsole.AddToConsole(msg.Get());
        }
        public void AddToPlayerConsole(IMessage msg)
        {
            playerConsole.AddToConsole(msg.Get());
        }

        public void AddInput(IMessage msg)
        {
            input.Enqueue(msg);
        }

        public IMessage GetInput()
        {
            return input.Count == 0 ? null : input.Dequeue();
        }
    }
    protected class Console
    {
        private readonly StringBuilder text;
        private readonly Text uiReference;

        public Console(Text uiReference)
        {
            this.uiReference = uiReference;
            text = new StringBuilder("");
        }

        public void AddToConsole(string toAdd)
        {
            if (uiReference == null) return;
            text.Append(toAdd+'\n');
            uiReference.text = text.ToString();
        }
    }

    protected class WinningCombination
    {
        public Card[] Cards;
        public CombinationType Type;

        public WinningCombination(CombinationType type, Card[] cards)
        {
            Cards = cards;
            Type = type;
        }

        public Global.Outcome CompareWith(WinningCombination other)
        {
            if (Type != other.Type)
                return Type < other.Type ? Global.Outcome.Win : Global.Outcome.Lost;

            switch (Type)
            {
                case CombinationType.StraightFlush:
                    if (Cards[0].value == other.Cards[0].value) return Global.Outcome.Tie;
                    return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                case CombinationType.FourOfaKind:
                    if (Cards[0].value == other.Cards[0].value)
                    {
                        if (Cards[4].value == other.Cards[4].value) return Global.Outcome.Tie;
                        return Cards[4].value > other.Cards[4].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    }
                    return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                case CombinationType.FullHouse:
                    if (Cards[0].value == other.Cards[0].value)
                    {
                        if (Cards[3].value == other.Cards[3].value) return Global.Outcome.Tie;
                        return Cards[3].value > other.Cards[3].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    }
                    return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                case CombinationType.Flush:
                    for (var i = 0; i < 5; ++i)
                        if (Cards[i].value > other.Cards[i].value) return Global.Outcome.Win;
                        else if (Cards[i].value < other.Cards[i].value) return Global.Outcome.Lost;
                    return Global.Outcome.Tie;
                case CombinationType.Straight:
                    if (Cards[0].value == other.Cards[0].value) return Global.Outcome.Tie;
                    return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                case CombinationType.ThreeOfaKind:
                    if (Cards[0].value != other.Cards[0].value) return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    for (var i = 3; i < 5; ++i)
                        if (Cards[i].value > other.Cards[i].value) return Global.Outcome.Win;
                        else if (Cards[i].value < other.Cards[i].value) return Global.Outcome.Lost;
                    return Global.Outcome.Tie;
                case CombinationType.TwoPair:
                    if (Cards[0].value != other.Cards[0].value) return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    if (Cards[2].value != other.Cards[2].value) return Cards[2].value > other.Cards[2].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    if (Cards[4].value != other.Cards[4].value) return Cards[4].value > other.Cards[4].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    return Global.Outcome.Tie;
                case CombinationType.OnePair:
                    if (Cards[0].value != other.Cards[0].value) return Cards[0].value > other.Cards[0].value ? Global.Outcome.Win : Global.Outcome.Lost;
                    for (var i = 2; i < 5; ++i)
                        if (Cards[i].value > other.Cards[i].value) return Global.Outcome.Win;
                        else if (Cards[i].value < other.Cards[i].value) return Global.Outcome.Lost;
                    return Global.Outcome.Tie;
                case CombinationType.HighCard:
                    for (var i = 0; i < 5; ++i)
                        if (Cards[i].value > other.Cards[i].value) return Global.Outcome.Win;
                        else if (Cards[i].value < other.Cards[i].value) return Global.Outcome.Lost;
                    return Global.Outcome.Tie;
                default:
                    return Global.Outcome.Tie;
            }
        }

        public string GetCards()
        {
            return Cards.Aggregate("", (current, card) => current + card.ToString()+" ");
        }

        public new string GetType()
        {
            switch (Type)
            {
                case CombinationType.StraightFlush:
                    return "Straight Flush";
                case CombinationType.FourOfaKind:
                    return "Four Of a Kind";
                case CombinationType.FullHouse:
                    return "Full House";
                case CombinationType.Flush:
                    return "Flush";
                case CombinationType.Straight:
                    return "Straight";
                case CombinationType.ThreeOfaKind:
                    return "Three Of a Kind";
                case CombinationType.TwoPair:
                    return "Two Pairs";
                case CombinationType.OnePair:
                    return "One Pair";
                case CombinationType.HighCard:
                    return "High Card";
                default:
                    return "Unknow";
            }
        }

        public enum CombinationType
        {
            StraightFlush = 0,
            FourOfaKind = 1,
            FullHouse = 2,
            Flush = 3,
            Straight = 4,
            ThreeOfaKind = 5,
            TwoPair = 6,
            OnePair = 7,
            HighCard = 8,
        } 
    }
}
