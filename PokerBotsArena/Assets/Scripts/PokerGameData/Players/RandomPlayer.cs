﻿using UnityEngine;
using UnityEngine.UI;

namespace PokerGameData.Players
{
    public class RandomPlayer : IPokerPlayer
    {
        public string Name { get; private set; }
        public IPokerGame Game { get; set; }

        public RandomPlayer(string name)
        {
            Name = name;
        }

        private int currentSmallBlind = 0;

        public void ReceiveMessage(IMessage message)
        {
            Game.ReceiveLogMessage(this,new StringMessage(message.GetRaw()));
            var msg = message.GetRaw().Split(' ');
            switch (msg[0])
            {
                case "SMALLBLIND":
                    currentSmallBlind = int.Parse(msg[1]);
                    var pot = int.Parse(msg[2]);
                    var decision = new CallDecision(this);
                    Game.ReceiveMessage(this,decision);
                    break;
                case "CALL":
                    var caller = msg[1];
                    if (caller == Name) break;
                    var callPot = int.Parse(msg[2]);
                    var canReact = bool.Parse(msg[3]);
                    if (canReact)
                    {
                        var reaction = new CheckDecision(this);
                        Game.ReceiveMessage(this, reaction);
                    }
                    break;
                case "CHECK":
                    var whoChecked = msg[1];
                    if (whoChecked == Name) break;
                    var reactionAviable = bool.Parse(msg[2]);
                    if (reactionAviable)
                    {
                        var reaction = new CheckDecision(this);
                        Game.ReceiveMessage(this, reaction);
                    }
                    break;
                case "RAISE":
                    var whoRaised = msg[1];
                    if (whoRaised == Name) break;
                    IMessage rm;
                    if (Random.value <= 0.5f)
                        rm = new CallDecision(this);
                    else
                        rm = new FoldDecision(this);
                    Game.ReceiveMessage(this, rm);
                    break;
                case "FLOP":
                    var nowActing = msg[4];
                    if (nowActing != Name) break;
                    IMessage cd;
                    if( Random.value <=0.5f )
                        cd = new CheckDecision(this);
                    else
                        cd = new RaiseDecision(this,8*currentSmallBlind);
                    Game.ReceiveMessage(this, cd);
                    break;
                case "TURN":
                    var nowActingTurn = msg[2];
                    if (nowActingTurn != Name) break;
                    var cdTurn = new CheckDecision(this);
                    Game.ReceiveMessage(this, cdTurn);
                    break;
                case "RIVER":
                    var nowActingRiver = msg[2];
                    if (nowActingRiver != Name) break;
                    IMessage cdRiver;
                    if (Random.value <= 0.5f)
                        cdRiver = new CheckDecision(this);
                    else
                        cdRiver = new RaiseDecision(this, 12 * currentSmallBlind);
                    Game.ReceiveMessage(this, cdRiver);
                    break;

            }
        }
    }
}
