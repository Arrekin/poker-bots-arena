﻿namespace PokerGameData
{
    public class Card
    {
        public readonly int value;

        public readonly Color color;

        public Card(int value, Color color)
        {
            this.value = value;
            this.color = color;
        }

        public Card(int value, int color)
        {
            this.value = value;
            switch (color)
            {
                case 0:
                    this.color = Color.Spade;
                    break;
                case 1:
                    this.color = Color.Club;
                    break;
                case 2:
                    this.color = Color.Heart;
                    break;
                case 3:
                    this.color = Color.Diamond;
                    break;
            }
        }

        public override string ToString()
        {
            return "" + value + (char)color;
        }


        public enum Color
        {
            Spade = 's',
            Club = 'c',
            Heart = 'h',
            Diamond = 'd'
        }
    }
}
