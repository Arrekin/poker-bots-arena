﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.UI;

public interface IPokerPlayer
{
    /// <summary>
    /// Name of Ai, Canno contains spaces
    /// </summary>
    string Name { get; }
    /// <summary>
    /// Reference to game instace
    /// </summary>
    IPokerGame Game { get; set; }
    /// <summary>
    /// Method that game instance will use to send message to player
    /// </summary>
    void ReceiveMessage(IMessage message);
}

public interface IPokerGame
{
    /// <summary>
    /// Method for sending data by players
    /// </summary>
    void ReceiveMessage(IPokerPlayer player, IMessage message);

    /// <summary>
    /// Method for logging message to player personal console
    /// </summary>
    void ReceiveLogMessage(IPokerPlayer player, IMessage message);
}

public interface IMessage
{
    string Get();
    string GetRaw();
}