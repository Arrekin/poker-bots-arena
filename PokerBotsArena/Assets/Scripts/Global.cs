﻿
public static class Global
{
    public enum ErrorType
    {
        WrongId = 0,
        Timeout = 1,
        EmptyMessage = 2,
        UnknowCommand = 3,
    }

    public enum Outcome
    {
        Win,
        Lost,
        Tie
    }
}